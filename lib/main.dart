import 'package:flutter/material.dart';
import 'dart:math';
import 'profile.dart';

void main(){
  runApp(new MaterialApp(
    home: new Home(),
    routes: <String, WidgetBuilder>{
      '/Kalkulator' : (BuildContext context) => new Home(),
      '/Profile' : (BuildContext context) => new MyProfil(),
    },
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  int tinggi = 0;
  int jari = 0;
  String _jk="";

  void _pilihJk(String value){
    setState(() {
      _jk = value;
    });
  }

  var _nomor = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: new Icon(Icons.menu, color : Colors.white),
        title: new Text('Menghitung Volume Tabung'),
        centerTitle: true,
        backgroundColor: Colors.lightBlue,
        actions: <Widget>[
          new IconButton(
            icon: Icon(Icons.person, color : Colors.white),
            onPressed: (){
              Navigator.pushNamed(context, '/Profile');
            },
          )
        ],
      ),

      body: ListView(
        children: <Widget>[
          new Container(
            padding: new EdgeInsets.all(20.0),
            child: new Column(
              children: <Widget>[
                new TextField(
                  controller: _nomor,
                  maxLength: 15,
                  decoration: new InputDecoration(
                      labelText: "Nama",
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0)
                      )
                  ),
                ),
                new Padding(padding: new EdgeInsets.only(top: 20.0)),
                new TextField(
                  onChanged: (txt) {
                    setState(() {
                      tinggi = int.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                      labelText: "Jari-Jari Tabung",
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0)
                      )
                  ),
                ),
                new Padding(padding: new EdgeInsets.only(top: 20.0)),
                new TextField(
                  onChanged: (txt) {
                    setState(() {
                      jari = int.parse(txt);
                    });
                  },
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                      labelText: "Tinggi Tabung",
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0)
                      )
                  ),
                ),
                new Padding(padding: new EdgeInsets.only(top: 20.0)),
                Container(
                  //height: double.infinity,
                  margin: EdgeInsets.only(left: 10,right: 10,bottom: 20),
                  child: RaisedButton(
                    onPressed: () {
                      var route = new MaterialPageRoute(
                        builder: (BuildContext) =>
                        new VOLUMEResult(nama : _nomor.text,Ttinggi: tinggi,Tjari: jari, jk: _jk),
                      );
                      Navigator.of(context).push(route);
                    },
                    padding: EdgeInsets.all(10.0),
                    color: Colors.lightGreen,
                    textColor: Colors.white,
                    child: Text(
                      'HITUNG',
                      style:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class VOLUMEResult extends StatelessWidget {
  VOLUMEResult({@required this.Ttinggi, @required this.nama, @required this.Tjari, @required this.jk});
  final int Ttinggi;
  final int Tjari;
  final String nama;
  final String jk;
  int tvolume;
  String cVOLUME;
  int volume;
  @override
  Widget build(BuildContext context) {
    double volume = (Tjari * Tjari * 3.14 * Ttinggi);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('HASIL'),
      ),
      body: Container(
        color: Colors.white,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 20,child: Container(
              color: Colors.white,
            ),),
            new Text(
              "Halo ${nama} ,",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w300,
                  color: Colors.black87
              ),
            ),
            SizedBox(height: 10,child: Container(
              color: Colors.white,
            ),),
            Text(
              'Hasil volume Tabung adalah :',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Colors.black54,
              ),
            ),
            Text(
              ' $volume ',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
